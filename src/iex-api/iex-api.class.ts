import { INewsItem, IStockSymbol, IPastDividend, IEXTimePeriod, IEXListType, IListStock, IPriceByMinute, IHistoricalPrice } from './iex-api.types';
import * as requestPromise from 'request-promise';
import { LoggingService } from '../logging/logging.service';

export class IEXApi {
    private static fileName = 'stock-api.ts';
    private static url = 'https://api.iextrading.com/1.0/';
    private static extensionStrings = {
        dividendHistory: (symbol: string, period: IEXTimePeriod) => `stock/${symbol}/dividends/${period}`,
        symbols: 'ref-data/symbols',
        news: 'stock/market/news/last/50',
        list: (listType: IEXListType) => `stock/market/list/${listType}`,
        priceByMinute: (symbol: string) => `stock/${symbol}/chart/1d`,
        priceByMinutePreviousDate: (symbol: string, date: string) => `stock/${symbol}/chart/date/${date}`, // Date must be YYYYMMDD format.
        priceByPeriod: (symbol: string, period: '5y' | '2y' | '1y' | 'ytd' | '6m' | '3m' | '1m') => `stock/${symbol}/chart/${period}`
    };

    /**
     * Makes a request and returns a promise of the IEX api response from the provided URL extension.
     * @param extensionString 
     * @returns Promise<any>
     */
    private static requestByExtension(extensionString: string): Promise<any> {
        const self = IEXApi;
        const encodedURL = encodeURI(`${self.url}${extensionString}`);
        return requestPromise({ uri: encodedURL, json: true });
    };

    /**
     * TODO: Add documentation
     */
    public static getNews(): Promise<INewsItem[]> {
        const self = IEXApi;
        return self.requestByExtension(self.extensionStrings.news)
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getNews()',
            error
        ));
    }

    /**
     * TODO: Add documentation
     */
    public static getAllSymbols(): Promise<IStockSymbol[]> {
        const self = IEXApi;
        return self.requestByExtension(self.extensionStrings.symbols)
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getAllSymbols()',
            error
        ));
    }
    

    /**
     *  TODO: Add documentation
     */
    public static getDividendHistory(symbol: string, period: IEXTimePeriod): Promise<IPastDividend[]> {
        const self = IEXApi;
        return self.requestByExtension(self.extensionStrings.dividendHistory(symbol, period))
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getDividendHistory()',
            error
        ));
    }


    /**
     * TODO: Add documentation
     * @param listType 
     */
    public static getList(listType: IEXListType): Promise<IListStock[]> {
        const self = IEXApi;
        return self.requestByExtension(self.extensionStrings.list(listType))
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getList()',
            error
        ));
    }


    /**
     * TODO: Add documentation for this
     * @param symbol 
     * @param date 
     */
    public static getPriceByMinute(symbol: string, date?: string): Promise<IPriceByMinute[]> {
        const self = IEXApi;
        const isDateProvidedAndValid = !!date && (date.length !== 8 || !isNaN(Number(date)) || date.indexOf('/') !== -1 || date.indexOf('-') !== -1);
        if (!isDateProvidedAndValid) console.error('stock-api.ts, getPriceByMinute(): Invalid date parameter! Date must be in YYYYMMDD format!');
        let priceByMinUrl = self.extensionStrings.priceByMinute(symbol);
        if (!!date) priceByMinUrl = self.extensionStrings.priceByMinutePreviousDate(symbol, date);
        return self.requestByExtension(priceByMinUrl)
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getPriceByMinute()',
            error
        ));
    }

    /**
     * TODO: Add documentation
     * @param symbol 
     * @param period 
     */
    public static getPriceHistory(symbol: string, period: IEXTimePeriod): Promise<IHistoricalPrice[]> {
        const self = IEXApi;
        return self.requestByExtension(self.extensionStrings.priceByPeriod(symbol, period))
        .catch((error: Error) => LoggingService.logError(
            self.fileName,
            'getPriceHistory()',
            error
        ));
    }
    

}