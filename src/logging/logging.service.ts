export class LoggingService {
    public static logError(fileName: string, functionName: string, error: Error) {
        console.error(
            `
            ${fileName} --> ${functionName}:\r\n\r\n
            Error Message: ${error.message}\r\n
            Error: \r\n
            ${error}
            `
        );
    }
}